<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Registro usuarios</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        
        <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
        
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            body {
                background: white;
            font-size:2em;
            }
            table
            {
                color: blue;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            table{
    background-color: #F5FFFA;
      color: black;
      position: absolute; 
      margin-top:100px;

      
}

th {
    background-color: #4CAF50;
   color:white;
}


th{
    
    color:white;
}
h2{
    color: white;
}

        </style>
    </head>
    <body>
     <td><a href="create" </a></td>
        <div class="content">
            <h1>Registro usuarios</h1>
            <table style="width:100%" class="table table-hover">
                <tr>
                    <th>ID</th>
                    <th>Codigo</th> 
                    <th>Razon social</th>
                    <th>Nombre</th>
                    <th>Pais</th> 
                    <th>Estado</th>
                    <th>Ciudad</th>
                    <th>Telefono</th>
                    <th>Correo</th>

                    <th colspan="2">Acciones</th>
                </tr>
                @foreach ($empresarios as $empresario)
                <tr>
                    <td>{{ $empresario->razonsocial}}</td>

                     <td>{{ $empresario->nombre}}</td>
                     <td>{{ $empresario->pais}}</td>
                     <td>{{ $empresario->estado}}</td>

                     <td>{{ $empresario->tiudad}}</td>

                      <td>{{ $empresario->telefono}}</td>
                       <td>{{ $empresario->tiudad}}</td>
                        <td>{{ $empresario->correo}}</td>

                        <td>{{ $empresario->activo}}</td>


                         <td><a href="edit&<?php echo  $empresario->id ?>"> Editar </a></td>
                     <td><a href="eliminar&<?php echo  $empresario->id ?>"> Eliminar </a></td>
                     </tr>


                    
                
                @endforeach

            </table>

           
    </body>
</html>
